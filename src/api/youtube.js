import axios from 'axios';

const KEY = 'AIzaSyC-KjOtLWv2ayZoLdlAWi1fJRN8xp6sr7w';

export default axios.create(
    {
        baseURL: 'https://www.googleapis.com/youtube/v3',
        params:
        {
            part: 'snippet',
            maxResults: 5,
            key: KEY
        }
    }
)