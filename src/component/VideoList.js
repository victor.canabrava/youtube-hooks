import React from 'react'
import VideoItem from './VideoItem';

const VideoList = ({videos, onVideoSelect}) =>
{
    const videoItems = videos.map(video => 
    {
        const id = video.id.kind === "youtube#video" ? video.id.videoId : video.id.channelId;

        return (<VideoItem key={id} video={video} onVideoSelect={onVideoSelect}/>);
    }
    )
    const videoList =
    (
        <div className="ui relaxed divided list">
            {videoItems}
        </div>
    )

    return videoList;
}

export default VideoList;