import React, { useState } from 'react'

const SearchBar = ({onFormSubmit}) =>
{
    const [value, setValue] = useState('');

    const onSubmit = e =>
    {
        e.preventDefault();
        onFormSubmit(value);
    }

    const searchBar =
    (
        <div className="ui segment search-bar">
            <form onSubmit={onSubmit} className="ui form">
                <div className="field">
                    <label>Video Search</label>
                    <input 
                        type="text" 
                        value={value} 
                        onChange={e => setValue(e.target.value)}
                    />
                </div>
            </form>
        </div>
    )

    return searchBar;
}

SearchBar.defaultParams =
{
    onSubmit: value => console.log(value)
}

export default SearchBar