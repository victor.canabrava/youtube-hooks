import React, { useEffect, useState } from 'react'
import useVideos from '../hooks/useVideos';
import SearchBar from './SearchBar'
import VideoDetail from './VideoDetail';
import VideoList from './VideoList';

const App = () =>
{
    const [videos, search] = useVideos('Dust in the wind');
    const [selectedVideo, setSelectedVideo] = useState(null);
    const [autoplay, setAutoplay] = useState('0');

    const onVideoSelect = video =>
    {
        setSelectedVideo(video);
        setAutoplay('1');
    }

    useEffect(() =>
    {
        setSelectedVideo(videos[0]);
        setAutoplay('0');
    }, [videos]);
    
    const app =
    (
        <div className="ui container">
            <SearchBar onFormSubmit={search}/>
            <div className="ui grid">
                <div className="ui row">       
                    <div className="eleven wide column">
                        <VideoDetail 
                            video={selectedVideo}
                            autoplay={autoplay}
                        />
                    </div>
                    <div className="five wide column">
                        <VideoList 
                            videos={videos} 
                            onVideoSelect={onVideoSelect}
                        />
                    </div>
                </div>
            </div>
        </div>
    )

    return app;
}

export default App;