import React from 'react'

const VideoDetail = ({video, autoplay}) =>
{
    if(!video) return (<div>Please, select a video</div>);
    if(!video.id.videoId) return (<div>The selected link was not a video</div>);

    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}?autoplay=${autoplay}`

    const videoDetail =
    (
        <div>
            <div className="ui embed">
            <iframe title="video player" src={videoSrc} frameborder="0" allow="autoplay" allowfullscreen="allowfullscreen"></iframe>
            </div>
            <div className="ui segment">
                <h4 className="ui header">{video.snippet.title}</h4>
                <p>{video.snippet.description}</p>
            </div>
        </div>
    )

    return videoDetail
}


export default VideoDetail;