import { useState, useEffect } from 'react';
import youtube from '../api/youtube';

const useVideos = (defaultTerm) =>
{
    const [videos, setVideos] = useState([]);

    useEffect(() =>
    {
        search(defaultTerm);
    }, [defaultTerm]);

    const search = async value =>
    {
        const result = await youtube.get('/search', {params: {q: value}});
        setVideos(result.data.items);
    }

    return [videos, search];
}

export default useVideos;